/*********************************************************************************/
/* Optimized sequential Matrix product                                           */
/* S. Vialle - january 2013                                                      */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "main.h"
#include "Init.h"


/*-------------------------------------------------------------------------------*/
/* Initialisation of local matrixes A, B and C                                   */
/*-------------------------------------------------------------------------------*/
void LocalMatrixInit(void)
{
 int i, j;                                /* Local matrix indexes                */

/* Initialization of the local matrix elements                                   */
 for (i = 0; i < SIZE; i++)
    for (j = 0; j < SIZE; j++)
       A[i][j] = (double) (i*SIZE + j);
 
 for (i = 0; i < SIZE; i++)
    for (j = 0; j < SIZE; j++){
       B[i][j] = (double) (i*SIZE + j);
       TP[j][i] = B[i][j];
    }		
 for (i = 0; i < SIZE; i++)
    for (j = 0; j < SIZE; j++)
       C[i][j] = 0.0;
}


/*-------------------------------------------------------------------------------*/
/* Command Line parsing.                                                         */
/*-------------------------------------------------------------------------------*/

void CommandLineParsing(int argc, char *argv[])
{
 if (argc > 1) {
   fprintf(stdout,
	   "usage: \t MatrixProduct\n");
   fflush(stdout);
   exit(0);
 }
}

