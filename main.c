/*********************************************************************************/
/* Optimized sequential Matrix product                                           */
/* S. Vialle - january 2013                                                      */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>

#include "main.h"
#include "Init.h"
#include "Calcul.h"


/*-------------------------------------------------------------------------------*/
/* Global variable declarations.                                                 */
/*-------------------------------------------------------------------------------*/

double A[SIZE][SIZE];                            /* Matrixes : C = A.B           */
double B[SIZE][SIZE];
double C[SIZE][SIZE];
double TP[SIZE][SIZE];


/*-------------------------------------------------------------------------------*/
/* Print result of the parallel computation and performances                     */
/*-------------------------------------------------------------------------------*/
void PrintResultsAndPerf(double megaflops, double d1,double d2)
{
   fprintf(stdout,"PE%d: Product of two square matrixes of %dx%d doubles:\n",
           0,SIZE,SIZE);
   fprintf(stdout,"\tPE%d: C[%d][%d] = %f\n",
           0,0,SIZE-1,
           (float) C[0][SIZE-1]);
   fprintf(stdout,"\tPE%d: C[%d][%d] = %f\n",
           0,SIZE/2,SIZE/2,
           (float) C[SIZE/2][SIZE/2]);
   fprintf(stdout,"\tPE%d: C[%d][%d] = %f\n",
           0,SIZE-1,0,
           (float) C[SIZE-1][0]);
   fprintf(stdout,"\n\tPE%d: Elapsed time of the loop = %f(s)\n",
           0,(float) d1);
   fprintf(stdout,"\tPE%d: Total Megaflops = %f\n",
           0,(float) megaflops);
   fflush(stdout);

   fprintf(stdout,"\n\tPE%d: Total elapsed time of the application = %f(s)\n",
           0,(float) d2);
   fflush(stdout);
}


/*-------------------------------------------------------------------------------*/
/* Toplevel function.                                                            */
/*-------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
    struct timeval td1, tf1;            /* Time measures of the computation loop */
    struct timeval td2, tf2;            /* Time measures of the entire programe  */
    struct timezone tz;
    double d1, d2;
    double megaflops;

	gettimeofday(&td2,&tz);             /* Begin of total time measurement       */
    CommandLineParsing(argc,argv);                /* For future evolutions ...   */
	LocalMatrixInit();                            /* Initialization of the data  */

	fprintf(stdout,"Sequential computation starts\n");
	fflush(stdout);

	gettimeofday(&td1,&tz);                       /* Begin of time measurement   */                    
	SeqLocalProduct();                            /* Parallel Matrix product     */
	gettimeofday(&tf1,&tz);                       /* End of the time measurement */
    gettimeofday(&tf2,&tz);             /* End of total time measurement         */

	                                    /* Performance computation and printing  */
	d1 = tf1.tv_sec - td1.tv_sec + (tf1.tv_usec - td1.tv_usec)*1E-6;
    megaflops = 2*pow(SIZE,3)/d1*1E-6;  
    d2 = tf2.tv_sec - td2.tv_sec + (tf2.tv_usec - td2.tv_usec)*1E-6;
	PrintResultsAndPerf(megaflops,d1,d2);         

	return(0);
}
