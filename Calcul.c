/*********************************************************************************/
/* Optimized sequential Matrix product                                           */
/* S. Vialle - january 2013                                                      */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "Calcul.h"
#include "cblas.h"

//#define normal
//#define order_ikj
//#define transpose
//#define array_acc
#define unroll_acc
//#define acc_withoutassigning
//#define cblas


/*-------------------------------------------------------------------------------*/
void SeqLocalProduct()
{
	int i,j,k;
#ifdef normal
	for(i=0; i<SIZE;++i)
		for(j=0;j<SIZE;++j)
			for(k=0;k<SIZE;++k)
				C[i][j]+=A[i][k]*B[k][j];

#endif

#ifdef order_ikj
	for(i=0; i<SIZE;++i)
		for(k=0;k<SIZE;++k)
			for(j=0;j<SIZE;++j)
				C[i][j]+=A[i][k]*B[k][j];

#endif

#ifdef transpose
	for(i=0; i<SIZE;++i)
		for(j=0;j<SIZE;++j)
			for(k=0;k<SIZE;++k)
				C[i][j]+=A[i][k]*TP[j][k];

#endif

#ifdef unroll_acc
	for(i=0; i<SIZE;++i)
		for(j=0;j<SIZE;++j){
			double acc = 0.0;
			for(k=0;k<SIZE;k+=4){
				acc+=A[i][k]*TP[j][k];
				acc+=A[i][k+1]*TP[j][k+1];
				acc+=A[i][k+2]*TP[j][k+2];
				acc+=A[i][k+3]*TP[j][k+3];
				/*acc+=A[i][k+4]*TP[j][k+4];
				  acc+=A[i][k+5]*TP[j][k+5];
				  acc+=A[i][k+6]*TP[j][k+6];
				  acc+=A[i][k+7]*TP[j][k+7];
				  acc+=A[i][k+8]*TP[j][k+8];
				  acc+=A[i][k+9]*TP[j][k+9];
				  acc+=A[i][k+10]*TP[j][k+10];
				  acc+=A[i][k+11]*TP[j][k+11];
				  acc+=A[i][k+12]*TP[j][k+12];
				  acc+=A[i][k+13]*TP[j][k+13];
				  acc+=A[i][k+14]*TP[j][k+14];
				  acc+=A[i][k+15]*TP[j][k+15];*/	
			}
			C[i][j]=acc;
		}
#endif

#ifdef array_acc
	for(i=0; i<SIZE;++i)
		for(j=0;j<SIZE;++j){
			double acc[4] = {0.0,0.0,0.0,0.0};
			for(k=0;k<SIZE;k+=4){
				acc[0]+=A[i][k]*TP[j][k];
				acc[1]+=A[i][k+1]*TP[j][k+1];
				acc[2]+=A[i][k+2]*TP[j][k+2];
				acc[3]+=A[i][k+3]*TP[j][k+3];
			}
			C[i][j]=acc[0]+acc[1]+acc[2]+acc[3];
		}
#endif

#ifdef acc_withoutassigning
	for(i=0; i<SIZE;++i)
		for(j=0;j<SIZE;++j){
			double acc = 0.0;
			for(k=0;k<SIZE;k+=4){
				acc+=A[i][k]*TP[j][k]+A[i][k+1]*TP[j][k+1]+A[i][k+2]*TP[j][k+2]+A[i][k+3]*TP[j][k+3];

			}
			C[i][j]=acc;
		}
#endif

#ifdef cblas
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
			SIZE, SIZE, SIZE,
			1.0, &A[0][0], SIZE,
			&B[0][0], SIZE,
			0.0, &C[0][0], SIZE);
#endif

}
