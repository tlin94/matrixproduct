CC=gcc
CFLAGS= -O3 -funroll-loops
LDFLAGS= -L/usr/lib64/atlas/
LIBS= -lm -lcblas -latlas
SOURCES= Calcul.c Init.c main.c 
EXECNAME= MatrixProduct

all:
	$(CC) $(CFLAGS) $(LDFLAGS) -I/usr/include/atlas -o $(EXECNAME) $(SOURCES) $(LIBS)

clean:
	rm -f *.o core


